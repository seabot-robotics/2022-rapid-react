// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.motorcontrol.Spark;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Robot;
import frc.robot.RobotConstants;

public class Blinkin extends SubsystemBase {
  // CONSTANTS
  //Fixed Palette Pattern Constants
  public static final double FIXED_PATTERN_RAINBOW_RAINBOW            = -0.99;
  public static final double FIXED_PATTERN_RAINBOW_PARTY              = -0.97;
  public static final double FIXED_PATTERN_RAINBOW_OCEAN              = -0.95;
  public static final double FIXED_PATTERN_RAINBOW_LAVA               = -0.93;
  public static final double FIXED_PATTERN_RAINBOW_FOREST             = -0.91;
  public static final double FIXED_PATTERN_RAINBOW_GLITTER            = -0.89;

  public static final double FIXED_PATTERN_CONFETTI                   = -0.87;

  public static final double FIXED_PATTERN_SHOT_RED                   = -0.85;
  public static final double FIXED_PATTERN_SHOT_BLUE                  = -0.83;
  public static final double FIXED_PATTERN_SHOT_WHITE                 = -0.81;
  public static final double FIXED_PATTERN_SINELON_RAINBOW            = -0.79;
  public static final double FIXED_PATTERN_SINELON_PARTY              = -0.77;
  public static final double FIXED_PATTERN_SINELON_OCEAN              = -0.75;
  public static final double FIXED_PATTERN_SINELON_LAVA               = -0.73;
  public static final double FIXED_PATTERN_SINELON_FOREST             = -0.71;
  public static final double FIXED_PATTERN_BEATS_PER_MINUTE_RAINBOW   = -0.69;
  public static final double FIXED_PATTERN_BEATS_PER_MINUTE_PARTY     = -0.67;
  public static final double FIXED_PATTERN_BEATS_PER_MINUTE_OCEAN     = -0.65;
  public static final double FIXED_PATTERN_BEATS_PER_MINUTE_LAVA      = -0.63;
  public static final double FIXED_PATTERN_BEATS_PER_MINUTE_FOREST    = -0.61;

  public static final double FIXED_PATTERN_FIRE_MEDIUM                = -0.59;
  public static final double FIXED_PATTERN_FIRE_LARGE                 = -0.57;

  public static final double FIXED_PATTERN_TWINKLES_RAINBOW           = -0.55;
  public static final double FIXED_PATTERN_TWINKLES_PARTY             = -0.53;
  public static final double FIXED_PATTERN_TWINKLES_OCEAN             = -0.51;
  public static final double FIXED_PATTERN_TWINKLES_LAVA              = -0.49;
  public static final double FIXED_PATTERN_TWINKLES_FOREST            = -0.47;

  public static final double FIXED_PATTERN_COLOR_WAVES_RAINBOW        = -0.45;
  public static final double FIXED_PATTERN_COLOR_WAVES_PARTY          = -0.43;
  public static final double FIXED_PATTERN_COLOR_WAVES_OCEAN          = -0.41;
  public static final double FIXED_PATTERN_COLOR_WAVES_LAVA           = -0.39;
  public static final double FIXED_PATTERN_COLOR_WAVES_FOREST         = -0.37;

  public static final double FIXED_PATTERN_LARSON_SCANNER_RED         = -0.35;
  public static final double FIXED_PATTERN_LARSON_SCANNER_GRAY        = -0.33;

  public static final double FIXED_PATTERN_LIGHT_CHASE_RED            = -0.31;
  public static final double FIXED_PATTERN_LIGHT_CHASE_BLUE           = -0.29;
  public static final double FIXED_PATTERN_LIGHT_CHASE_GRAY           = -0.27;

  public static final double FIXED_PATTERN_HEARTBEAT_RED              = -0.25;
  public static final double FIXED_PATTERN_HEARTBEAT_BLUE             = -0.23;
  public static final double FIXED_PATTERN_HEARTBEAT_WHITE            = -0.21;
  public static final double FIXED_PATTERN_HEARTBEAT_GRAY             = -0.19;

  public static final double FIXED_PATTERN_BREATH_RED                 = -0.17;
  public static final double FIXED_PATTERN_BREATH_BLUE                = -0.15;
  public static final double FIXED_PATTERN_BREATH_GRAY                = -0.13;

  public static final double FIXED_PATTERN_STROBE_RED                 = -0.11;
  public static final double FIXED_PATTERN_STROBE_BLUE                = -0.09;
  public static final double FIXED_PATTERN_STROBE_GOLD                = -0.07;
  public static final double FIXED_PATTERN_STROBE_WHITE               = -0.05;

  // Color 1 Patterns
  public static final double COLOR_1_PATTERN_END_TO_END_BLEND_BLACK = -0.03;
  public static final double COLOR_1_PATTERN_LARSON_SCANNER         = -0.01;
  public static final double COLOR_1_PATTERN_LIGHT_CHASE            = 0.01;
  public static final double COLOR_1_PATTERN_HEARTBEAT_SLOW         = 0.03;
  public static final double COLOR_1_PATTERN_HEARTBEAT_MEDIUM       = 0.05;
  public static final double COLOR_1_PATTERN_HEATBEAT_FAST          = 0.07;
  public static final double COLOR_1_PATTERN_BREATH_SLOW            = 0.09;
  public static final double COLOR_1_PATTERN_BREATH_FAST            = 0.11;
  public static final double COLOR_1_PATTERN_SHOT                   = 0.13;
  public static final double COLOR_1_PATTERN_STROBE                 = 0.15;

  // Color 2 Patterns
  public static final double COLOR_2_PATTERN_END_TO_END_BLEND_BLACK = 0.17;
  public static final double COLOR_2_PATTERN_LARSON_SCANNER         = 0.19;
  public static final double COLOR_2_PATTERN_LIGHT_CHASE            = 0.21;
  public static final double COLOR_2_PATTERN_HEARTBEAT_SLOW         = 0.23;
  public static final double COLOR_2_PATTERN_HEARTBEAT_MEDIUM       = 0.25;
  public static final double COLOR_2_PATTERN_HEARTBEAT_FAST         = 0.27;
  public static final double COLOR_2_PATTERN_BREATH_SLOW            = 0.29;
  public static final double COLOR_2_PATTERN_BREATH_FAST            = 0.31;
  public static final double COLOR_2_PATTERN_SHOT                   = 0.33;
  public static final double COLOR_2_PATTERN_STROBE                 = 0.35;

  // Color 1 & 2 Patterns
  public static final double COLOR_1_AND_2_PATTERN_SPARKLE_1_ON_2           = 0.37;
  public static final double COLOR_1_AND_2_PATTERN_SPARKLE_2_ON_1           = 0.39;

  public static final double COLOR_1_AND_2_PATTERN_COLOR_GRADIENT           = 0.41;
  public static final double COLOR_1_AND_2_PATTERN_BEATS_PER_MINUTE         = 0.43;
  public static final double COLOR_1_AND_2_PATTERN_END_TO_END_BLEND_1_TO_2  = 0.45;
  public static final double COLOR_1_AND_2_PATTERN_END_TO_END_BLEND         = 0.47;
  public static final double COLOR_1_AND_2_PATTERN_NO_BLENDING              = 0.49;
  public static final double COLOR_1_AND_2_PATTERN_TWINKLES                 = 0.51;
  public static final double COLOR_1_AND_2_PATTERN_COLOR_WAVES              = 0.53;
  public static final double COLOR_1_AND_2_PATTERN_SINELON                  = 0.55;

  // Solid Color Constants
  public static final double COLOR_HOT_PINK           = 0.57;
  public static final double COLOR_DARK_RED           = 0.59;
  public static final double COLOR_RED                = 0.61;
  public static final double COLOR_RED_ORANGE         = 0.63;
  public static final double COLOR_ORANGE             = 0.65;
  public static final double COLOR_GOLD               = 0.67;
  public static final double COLOR_YELLOW             = 0.69;
  public static final double COLOR_LAWN_GREEN         = 0.71;
  public static final double COLOR_LIME               = 0.73;
  public static final double COLOR_DARK_GREEN         = 0.75;
  public static final double COLOR_GREEN              = 0.77;
  public static final double COLOR_BLUE_GREEN         = 0.79;
  public static final double COLOR_AQUA               = 0.81;
  public static final double COLOR_SKY_BLUE           = 0.83;
  public static final double COLOR_DARK_BLUE          = 0.85;
  public static final double COLOR_BLUE               = 0.87;
  public static final double COLOR_BLUE_VIOLET        = 0.89;
  public static final double COLOR_VIOLET             = 0.91;
  public static final double COLOR_WHITE              = 0.93;
  public static final double COLOR_GRAY               = 0.95;
  public static final double COLOR_DARK_GRAY          = 0.97;
  public static final double COLOR_BLACK              = 0.99;   // Black is equivalent to "Off"

  // Member variables
  private Spark   blinkinController = new Spark (RobotConstants.BLINKIN_PWM_CHANNEL);
  private double  currentColor;
  private double  primaryColor      = COLOR_WHITE;
  private double  alternateColor    = COLOR_BLACK;
  private int     interval          = 100;
  private int     count             = 0; 
  private boolean blink             = false;
  
  /**
   * Creates a new Blinkin.
   */
  public Blinkin(){
    this(COLOR_WHITE, COLOR_BLACK);
  }

  public Blinkin(double primaryColor){
    this(primaryColor, COLOR_BLACK);
  }
  
  public Blinkin(double primaryColor, double alternateColor){
    this.primaryColor   = primaryColor;
    this.alternateColor = alternateColor;
    currentColor        = primaryColor;
  }

  /**
   * Member functions
   */
  public void setPrimaryColor (double color){
    this.primaryColor = color;
    //blinkinController.set(currentValue);
  }

  public void setAlternateColor(double color){
    this.alternateColor = color;
  }

  public void setInterval(int interval){
    this.interval = interval;
  }

  public void setBlink(boolean blink){
    this.blink = blink;
  }

  @Override
  public void periodic() {
    if (this.getCurrentCommand() != null)
    SmartDashboard.putString("Blinkin Command", this.getCurrentCommand().getName());
    else
    SmartDashboard.putString("Blinkin Command", "NO COMMAND SCHEDULED");
    
    // This method will be called about every 50 ms...
    if (this.blink == true) {                                       // set to blink...
      if (this.interval > 0 && this.count % this.interval == 0){    // time to Switch colors
        if (this.currentColor == this.primaryColor){                // switch to alternate color
          this.currentColor = this.alternateColor;
        }
        else{                                                       // switch back to primary color
          this.currentColor = this.primaryColor;
        } 
      }
      count++;                                                      // increment loop count 
    }
    else {                                                          // Not blinking... set to primary color...
      this.currentColor = this.primaryColor;            
    }

    this.blinkinController.set(this.currentColor);                  // Set the color on the contorller
  }
}
