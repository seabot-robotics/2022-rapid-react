/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import java.util.Map;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import edu.wpi.first.wpilibj.motorcontrol.Talon;
import edu.wpi.first.wpilibj.motorcontrol.Victor; 
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import frc.robot.RobotConstants;
import frc.robot.commands.drivetrain.DriveHeading;
import frc.robot.commands.drivetrain.DriveHeadingTest;
import frc.robot.commands.drivetrain.DriveSimpleArcToHeading;
import frc.robot.commands.drivetrain.ResetEncoders;
import frc.robot.commands.drivetrain.TurnToAngle;
import frc.robot.commands.drivetrain.ZeroGyro;

public class DriveTrain extends SubsystemBase {

  // ***** Members *****
  private DifferentialDrive differentialDrive; // WPILib Drive Train object - Supports Arcade & Tank modes

  private ShuffleboardTab driveTrainDashboardTab = Shuffleboard.getTab("DriveTrain");
  private ShuffleboardLayout driveArc = driveTrainDashboardTab.getLayout("Drive Simple Arc", BuiltInLayouts.kList).withProperties(Map.of("Label Position", "TOP")).withPosition(8, 0).withSize(2, 3);
  private ShuffleboardLayout leftMotors = driveTrainDashboardTab.getLayout("Left Motors", BuiltInLayouts.kList).withProperties(Map.of("Label Position", "HIDDEN")).withPosition(2, 2).withSize(2, 2);
  private ShuffleboardLayout rightMotors = driveTrainDashboardTab.getLayout("Right Motors", BuiltInLayouts.kList).withProperties(Map.of("Label Position", "HIDDEN")).withPosition(4, 2).withSize(2, 2);
  private ShuffleboardLayout gyroLayout = driveTrainDashboardTab.getLayout("Gyroscope", BuiltInLayouts.kList).withProperties(Map.of("Label Position", "HIDDEN")).withPosition(0, 0).withSize(2, 2);
  private ShuffleboardLayout navCmdLayout = driveTrainDashboardTab.getLayout("Nav Commands", BuiltInLayouts.kList).withProperties(Map.of("Label Position", "HIDDEN")).withPosition(0, 2).withSize(2, 2);

  private ShuffleboardTab cmdTab = Shuffleboard.getTab("Commands");
  private ShuffleboardLayout driveCmdLayout = cmdTab.getLayout("Drive Command", BuiltInLayouts.kList).withProperties(Map.of("Label Position", "HIDDEN")).withPosition(0, 0).withSize(2, 3);

  // **** NetworkTables for dashboard widgets
  private NetworkTableEntry maxSpeed;
  private NetworkTableEntry arcForwardSpeed;  
  private NetworkTableEntry arcTurnSpeed;
  private NetworkTableEntry arcDegrees;
  private NetworkTableEntry driveDistance;
  private NetworkTableEntry driveHeading;
  private NetworkTableEntry driveSpeed;

    // --- Drive Train motor speed controllers
    // Main Speed Controllers - Dev board & Dev Robot use 1 controller/side - Competition Robot uses 2/side (VictorSPX)
    private MotorController leftMotor;                       // Left devboard motor
    private MotorController rightMotor;                      // right devboard motor
    
    // Speed Controllers used on actual Robot
    private WPI_VictorSPX leftFollowMotor;
    private WPI_VictorSPX rightFollowMotor;

    public enum MotorType {
      Victor, Talon, VictorSPX
    };
    private MotorType motorType;                      // Indicates which type of speed controller we are using - Set in the contructor

    private ADXRS450_Gyro gyro = new ADXRS450_Gyro(); // Gyro for stablized driving and driving on heading in autonomous

    // Encoders for the driveTrain
    private Encoder leftEncoder = new Encoder(RobotConstants.DRIVE_TRAIN_LEFT_ENCODER_A, RobotConstants.DRIVE_TRAIN_LEFT_ENCODER_B);
    private Encoder rightEncoder = new Encoder(RobotConstants.DRIVE_TRAIN_RIGHT_ENCODER_A, RobotConstants.DRIVE_TRAIN_RIGHT_ENCODER_B);
    private boolean useLeftEncoder = true;
    private boolean useRightEncoder = true;



    /**
     * Creates a new DriveTrain.
     */
    public DriveTrain(MotorType motorType) {
        this.motorType = motorType;
        switch (motorType) {

            case Victor:
                leftMotor = new Victor(RobotConstants.LEFT_DEV_DRIVE_PWM);
                rightMotor = new Victor(RobotConstants.RIGHT_DEV_DRIVE_PWM);
                break;

            case Talon:
                leftMotor = new Talon (RobotConstants.LEFT_DEV_DRIVE_PWM);
                rightMotor  = new Talon (RobotConstants.RIGHT_DEV_DRIVE_PWM);
                break;

            case VictorSPX:
                leftMotor = new WPI_VictorSPX (RobotConstants.LEFT_LEAD_MOTOR_CAN_ID);
                rightMotor = new WPI_VictorSPX(RobotConstants.RIGHT_LEAD_MOTOR_CAN_ID);
                leftFollowMotor = new WPI_VictorSPX (RobotConstants.LEFT_FOLLOW_MOTOR_CAN_ID);
                rightFollowMotor = new WPI_VictorSPX(RobotConstants.RIGHT_FOLLOW_MOTOR_CAN_ID);
                
                leftFollowMotor.follow ((WPI_VictorSPX) leftMotor);
                rightFollowMotor.follow ((WPI_VictorSPX) rightMotor);

                rightFollowMotor.setInverted(true); // This is a change for 2022 - in previous years this was not needed, but it is now needed in 2022.
                break;
        }
        rightMotor.setInverted(true); // This is a change for 2022 - in previous years this was not needed, but it is now needed in 2022.

        differentialDrive = new DifferentialDrive(leftMotor, rightMotor);

        // Set Encoder Values
        leftEncoder.setDistancePerPulse (RobotConstants.DRIVE_TRAIN_WHEEL_CIRCUMFERNCE / RobotConstants.DRIVE_TRAIN_ENCODER_PULSES_PER_REV);
        rightEncoder.setDistancePerPulse(RobotConstants.DRIVE_TRAIN_WHEEL_CIRCUMFERNCE / RobotConstants.DRIVE_TRAIN_ENCODER_PULSES_PER_REV);
        resetEncoders();

        rightEncoder.setReverseDirection(true);
        leftEncoder.setReverseDirection(true);

        // Put the DriveTrain and Gyro objects on the SmartDashboard
        SmartDashboard.putData("Drive Train", differentialDrive);
        SmartDashboard.putData("Gyro", this.gyro);
        initializeDashboardTab();
    }

    // Default Constructor - Gets Default Speed Controller type from RobotConstants - Change Constant to change controller type.
    public DriveTrain() {
        this(RobotConstants.DEFAULT_DRIVETRAIN_CONTROLLER_TYPE);
    }

    public DriveTrain (boolean useLeftEncoder, boolean useRightEncoder, MotorType motorType){
      this(motorType);
      this.useLeftEncoder = useLeftEncoder;
      this.useRightEncoder = useRightEncoder;
    }

    public DriveTrain (boolean useLeftEncoder, boolean useRightEncoder){
      this (useLeftEncoder, useRightEncoder, RobotConstants.DEFAULT_DRIVETRAIN_CONTROLLER_TYPE);
    }

    /**
     * Set the Left Side motors
     * 
     * @param value - Value to set the motors to, range is -1.0 to 1.0.  If outside the range, will be clipped to -1.0 or 1.0 appropriately.
     * 
     * NOTE: Since was are now using WPI_VICTORSPX for the VictorSPX controllers, this is not really necessary, and not used.
     */
    public void setLeftMotor(double value) {
    switch (motorType){
      case Victor:
      case Talon:
        leftMotor.set(value);
        break;
      case VictorSPX:
        ((WPI_VictorSPX)leftMotor).set(ControlMode.PercentOutput, value);
        break;
    }
  }

  /**
   *  Set the Right side motors
   * 
   * @param value - Value to set the motors to, range is -1.0 to 1.0.  If outside the range, will be clipped to -1.0 or 1.0 appropriately.
   * 
   * NOTE: Since was are now using WPI_VICTORSPX for the VictorSPX controllers, this is not really necessary, and not used. 
   */
  public void setRightMotor(double value){
    switch (motorType){
      case Victor:
      case Talon:
        rightMotor.set(-value);
        break;
      case VictorSPX:
        ((WPI_VictorSPX)rightMotor).set(ControlMode.PercentOutput, -value);
        break;
    }
    SmartDashboard.putNumber("Right Motor", value);
  } 
  
  /**
   * Set drive train values using tank drive mode
   * 
   * @param leftValue   - Motor Value for the Left Side motors.  Range is -1.0 to 1.0.  DifferentialDrive will clip values outside the range
   * @param rightValue  - Motor Value for the Right Side motors.  Range is -1.0 to 1.0.  DifferenialDrive will clip values outside the range
   * 
   */
  public void setTankValues(double leftValue, double rightValue){
    // System.out.println("Left: " + leftValue + " Right: " + rightValue);
    differentialDrive.tankDrive(leftValue, rightValue);
  }

  public void setTankValues(double leftValue, double rightValue, boolean squareInputs){
    differentialDrive.tankDrive(leftValue, rightValue, squareInputs);
  }

  /**
   * Set drive train values using Arcade Drive mode
   * 
   * @param yValue  - Value to set in the Y direction (Forward/Backward).  Range is -1.0 to 1.0, and DifferentialDrive will clip values outside the range
   * @param xValue  - Value to set for the X direction (rotational/sideways).  Range is -1.0 to 1.0 and DifferentialDrive will clip values outside the range
   */
  public void setArcadeValues(double yValue, double xValue){
      differentialDrive.arcadeDrive(yValue, xValue);
  }

  public void setArcadeValues(double yValue, double xValue, boolean squareInputs){
    differentialDrive.arcadeDrive(yValue, xValue, squareInputs);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run - about every 50 ms.
    this.differentialDrive.setMaxOutput(maxSpeed.getDouble(1));

  }

  /**
   * Sets the max output of the drive. Useful for scaling the drive to drive more slowly.
   *
   * @param maxOutput the maximum output to which the drive will be constrained
   */
  public void setMaxOutput(double maxOutput) {
    this.differentialDrive.setMaxOutput(maxOutput);
  }

  public void setMaxOutput(){
    setMaxOutput(maxSpeed.getDouble(1.0));
  }

  // ***** Gyro methods ******
  /** 
   * Sets the Gyro to Zero. 
   */
  public void zeroHeading() {
    this.gyro.reset();
  }

  /**
   * Returns the heading of the robot from the Gyro.
   *
   * @return the robot's heading in degrees, from 180 to 180
   */
  public double getHeading() {
    return Math.IEEEremainder(this.gyro.getAngle(), 360) * (RobotConstants.kGyroReversed ? -1.0 : 1.0);
  }

  /**
   * Returns the turn rate of the robot.
   *
   * @return The turn rate of the robot, in degrees per second
   */
  public double getTurnRate() {
    return this.gyro.getRate() * (RobotConstants.kGyroReversed ? -1.0 : 1.0);
  }

  // **** Encoder Methods ****
  /**
   * Get the Distance the encoder has travelved.  This averages the two encoders, and is based on distance/pulse
   * 
   * @return distance traveled in units used to scale distance/pulse
   */
  public double getEncoderDistance(){
    double returnValue = 0.0;
    if (useLeftEncoder && useRightEncoder){
      returnValue = (leftEncoder.getDistance() + rightEncoder.getDistance())/2;
    }
    else if (useLeftEncoder){
      returnValue = leftEncoder.getDistance();
    }
    else{ // Using just the right encoder
      returnValue = rightEncoder.getDistance();
    }
    
    return returnValue;
  }

  /**
   * Resets the encoder counts to zero
   */
  public void resetEncoders(){
    leftEncoder.reset();
    rightEncoder.reset();
  }

  /**
   * Get the speed the encoders are traveling.  This averages the two encoders and is based on the distance/pulse.
   * 
   * @return distance unit / second in the units used to scale the encoder
   * 
   */
  public double getSpeed(){
    double returnValue = 0.0;
    if (useLeftEncoder && useRightEncoder){
      returnValue = (leftEncoder.getRate() + rightEncoder.getRate())/2;
    }
    else if (useLeftEncoder){
      returnValue = leftEncoder.getRate();
    }
    else{ // Using just the right encoder
      returnValue = rightEncoder.getRate();
    }

    return returnValue;
  }

  // ****** Dashboard stuff *****
  private void initializeDashboardTab(){
    maxSpeed = driveTrainDashboardTab.add("Max Speed", 1).withWidget(BuiltInWidgets.kNumberSlider).withProperties(Map.of("min",0, "max", 1)).withPosition(6, 1).getEntry();

    driveTrainDashboardTab.add("Differential Drive", differentialDrive).withPosition(2, 0).withSize(4, 2);
    gyroLayout.add("Gyro", gyro);//.withPosition(0, 0);
    gyroLayout.add("Zero Gyro", new ZeroGyro(this)).withWidget(BuiltInWidgets.kCommand);//.withPosition(9, 2);
    
    //leftMotors.add("Left Victor", leftMotor).withWidget(BuiltInWidgets.kMotorController);
    //rightMotors.add("Right Victor", rightMotor).withWidget(BuiltInWidgets.kMotorController);

    leftMotors.add("Left Encoder", leftEncoder);
    leftMotors.addBoolean("Use Left Encoder", this::getUseLeftEncoder);
    leftMotors.addNumber("Left PWM", this::getLeftMotorValue).withWidget(BuiltInWidgets.kNumberBar);

    rightMotors.add("Right Encoder", rightEncoder);
    rightMotors.addBoolean("Use Right Encoder", this::getUseRightEncoder);
    rightMotors.addNumber("Right PWM", this::getRightMotorValue).withWidget(BuiltInWidgets.kNumberBar);

    // navCmdLayout.add("Drive Heading", new DriveHeading(1.0, 0.0, this)).withWidget(BuiltInWidgets.kCommand);//.withPosition(8, 0);
    // navCmdLayout.add("Turn 15", new TurnToAngle(15, this)).withWidget(BuiltInWidgets.kCommand);//.withPosition(8, 1);
    // navCmdLayout.add("Reset Encoders", new ResetEncoders(this)).withWidget(BuiltInWidgets.kCommand);//.withPosition(8, 2);
    
    

    // //  Arc widgets... Thinking about if they should be on their own tab or not
    // arcForwardSpeed = driveArc.add("Arc - Forward Speed", 0).withWidget(BuiltInWidgets.kNumberSlider).withProperties(Map.of("min", -1, "max", 1)).getEntry();
    // arcTurnSpeed = driveArc.add("Arc - Turn Speed", 0).withWidget(BuiltInWidgets.kNumberSlider).withProperties(Map.of("min", -1, "max", 1)).getEntry();
    // arcDegrees = driveArc.add("Arc - Degrees", 0).withWidget(BuiltInWidgets.kNumberSlider).withProperties(Map.of("min", -360, "max", 360)).getEntry();
    // driveArc.add("Drive Arc", new DriveSimpleArcToHeading(this));

    // driveDistance = driveCmdLayout.add("Distance", 0).withWidget(BuiltInWidgets.kNumberSlider).withProperties(Map.of("min", 0, "max", 50)).getEntry();
    // driveHeading = driveCmdLayout.add("Heading", 0).withWidget(BuiltInWidgets.kNumberSlider).withProperties(Map.of("min", -360, "max", 360)).getEntry();
    // driveSpeed = driveCmdLayout.add("Speed", 0).withWidget(BuiltInWidgets.kNumberSlider).withProperties(Map.of("min", -1, "max", 1)).getEntry();
    // driveCmdLayout.add("Drive Heading", new DriveHeadingTest(driveSpeed.getDouble(0), driveHeading.getDouble(0), driveDistance.getDouble(0), this));
  }

  public ShuffleboardTab getShuffleboardTab(){
    return driveTrainDashboardTab;
  }

  public double getArcForwardSpeed(){
    return arcForwardSpeed.getDouble(0);
  }

  public double getArcTurnSpeed(){
    return arcTurnSpeed.getDouble(0);
  }

  public double getArcDegrees(){
    return arcDegrees.getDouble(0);
  }

  public boolean getUseLeftEncoder(){return useLeftEncoder;}
  public boolean getUseRightEncoder(){return useRightEncoder;}

  public double getDriveHeading(){return driveHeading.getDouble(0.0);}
  public double getDriveSpeed(){return driveSpeed.getDouble(0.0);}
  public double getDriveDistance(){return driveDistance.getDouble(0.0);}

  // Used for motor widgets on dasboard
  public double getLeftMotorValue(){return leftMotor.get();}
  public double getRightMotorValue(){return rightMotor.get();}
}
