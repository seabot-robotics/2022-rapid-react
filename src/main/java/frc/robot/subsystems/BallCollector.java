// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import java.util.Map;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.motorcontrol.Spark;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotConstants;
import frc.robot.RobotContainer;
import frc.robot.commands.BallCollector.LowerBallCollector;
import frc.robot.commands.BallCollector.RaiseBallCollector;
import frc.robot.commands.BallCollector.RunIntakeMotor;
import frc.robot.commands.BallCollector.RunQueuingMotor;
import frc.robot.commands.BallCollector.StopIntakeMotor;
import frc.robot.commands.BallCollector.StopQueuingMotor;

public class BallCollector extends SubsystemBase {
  // ** Motors
  private Spark intakeMotor = new Spark(RobotConstants.BALL_INTAKE_PWM_PORT);
  private Spark queuingMotor = new Spark(RobotConstants.BALL_QUEUER_PWM_PORT);

  private boolean intakeMotorRunning = false;
  private boolean queuingMotorRunning = false;

  // ** Pnuematics
  private Compressor compressor = null;
  private DoubleSolenoid leftCylinder = new DoubleSolenoid(PneumaticsModuleType.CTREPCM, RobotConstants.BALL_COLLECTOR_CYLINDER_LEFT_CHANNEL1, RobotConstants.BALL_COLLECTOR_CYLINDER_LEFT_CHANNEL2);
  private DoubleSolenoid rightCylinder = new DoubleSolenoid(PneumaticsModuleType.CTREPCM, RobotConstants.BALL_COLLECTOR_CYLINDER_RIGHT_CHANNEL1, RobotConstants.BALL_COLLECTOR_CYLINDER_RIGHT_CHANNEL2);

  // ** Dashboard stuff
  private NetworkTableEntry intakeMotorSpeed;
  private NetworkTableEntry queuingMotorSpeed;

  private ShuffleboardTab ballCollectorTab = Shuffleboard.getTab("Ball Collector");

  private ShuffleboardLayout ballIntakeLayout = ballCollectorTab.getLayout("Intake Motor", BuiltInLayouts.kList).withProperties(Map.of("Label Position", "HIDDEN")).withSize(2, 2).withPosition(0, 0);
  private ShuffleboardLayout ballQueuingLayout = ballCollectorTab.getLayout("Queuing Motor", BuiltInLayouts.kList).withProperties(Map.of("Label Position", "HIDDEN")).withSize(2, 2).withPosition(2, 0);

  private ShuffleboardLayout ballCollectorLayout = ballCollectorTab.getLayout("Pneumatics", BuiltInLayouts.kList).withProperties(Map.of("Label Position", "HIDDEN")).withSize(2, 2).withPosition(4, 0);

  /** Creates a new BallCollector. */
  public BallCollector() {
    leftCylinder.set(Value.kOff);
    rightCylinder.set(Value.kOff);

    initializeDashboardTab();
  }

  public BallCollector (Compressor compressor){
    this();
    this.compressor = compressor;
  }
  
  public boolean isIntakeMotorRunning() {
    if (intakeMotorRunning) {
      return true;
    } 
    else {
      return false;
    }
  }

  public boolean isQueingMotorRunning(){
    if (queuingMotorRunning){
      return true; 
    }
    else{
      return false; 
    }

  }

  private void initializeDashboardTab(){
    intakeMotorSpeed = ballIntakeLayout.add("Speed", 1).withWidget(BuiltInWidgets.kNumberSlider).withProperties(Map.of("min", -1, "max", 1)).getEntry();
    ballIntakeLayout.add("Start", new RunIntakeMotor(this, "Start Intake")).withWidget(BuiltInWidgets.kCommand);
    ballIntakeLayout.add("Stop", new StopIntakeMotor(this, "Stop Intake")).withWidget(BuiltInWidgets.kCommand);
    ballIntakeLayout.addNumber("Motor Value", this::getIntakeMotorValue).withWidget(BuiltInWidgets.kNumberBar).withProperties(Map.of("min", -1, "max", 1));

    queuingMotorSpeed = ballQueuingLayout.add("Speed", RobotConstants.BALL_COLLECTIOR_QUEUING_MOTOR_DEFAULT_SPEED).withWidget(BuiltInWidgets.kNumberSlider).withProperties(Map.of("min", -1, "max", 1)).getEntry();
    ballQueuingLayout.add("Start", new RunQueuingMotor(this, "Start Queuer")).withWidget(BuiltInWidgets.kCommand);
    ballQueuingLayout.add("Stop", new StopQueuingMotor(this, "Stop Queuer")).withWidget(BuiltInWidgets.kCommand);
    ballQueuingLayout.addNumber("Motor Value", this::getQueuingMotorValue).withWidget(BuiltInWidgets.kNumberBar).withProperties(Map.of("min", -1, "max", 1));

    ballCollectorLayout.add("Raise", new RaiseBallCollector(this, "Raise Collector")).withWidget(BuiltInWidgets.kCommand);
    ballCollectorLayout.add("Lower", new LowerBallCollector(this, "Lower Collector")).withWidget(BuiltInWidgets.kCommand);
    ballCollectorLayout.addBoolean("Left Extend", this::isLeftExtended);
    ballCollectorLayout.addBoolean("Right Extend", this::isRightExtended);

    ballCollectorTab.addBoolean("Compressor",  this::isCompressorRunning).withPosition(6, 0);
    ballCollectorTab.addBoolean("Pressure Switch", this::getPressureSwitch).withPosition(6, 1);


  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    if (queuingMotorRunning){
      queuingMotor.set(queuingMotorSpeed.getDouble(1));
    }

    if (intakeMotorRunning){
      intakeMotor.set(intakeMotorSpeed.getDouble(1));
    }
  }

  // ** INTAKE MOTOR FUNCTIONS
  public void startIntakeMotor(double speed){
    intakeMotor.set(speed);
    intakeMotorRunning = true;

  }
  public void startIntakeMotor(){
    startIntakeMotor(intakeMotorSpeed.getDouble(1));

  }

  public void stopIntakeMotor(){
    intakeMotor.set(0.0);
    intakeMotorRunning = false;
  }

  public double getIntakeMotorValue(){return intakeMotor.get();}

  // ** QUEUING MOTOR FUNCTIONS
  public void startQueuingMotor(double speed){
    queuingMotor.set(speed);
    queuingMotorRunning = true;
  }

  public void startQueuingMotor(){
    startQueuingMotor(queuingMotorSpeed.getDouble(1));

  }

  public void stopQueuingMotor(){
    queuingMotor.set(0.0);
    queuingMotorRunning = false;
  }

  public double getQueuingMotorValue(){return queuingMotor.get();}

  // ** Pnuematics fuctions
  public void raiseCollector(){
    leftCylinder.set(Value.kReverse);
    rightCylinder.set(Value.kReverse);

  }

  public void lowerCollector(){
    leftCylinder.set(Value.kForward);
    rightCylinder.set(Value.kForward);
  }

  public boolean isLeftExtended(){
    boolean returnValue = false;

    returnValue = leftCylinder.get() == Value.kForward;

    return returnValue;
  }

  public boolean isRightExtended(){
    boolean returnValue = false;

    returnValue = rightCylinder.get() == Value.kForward;

    return returnValue;
  }

  public boolean isCompressorRunning(){
    boolean returnValue = false;
    if (compressor != null){
      returnValue = compressor.enabled();
    } 
    return returnValue;
  }

  public boolean getPressureSwitch(){
    boolean returnValue = false;

    if(compressor != null){
      returnValue = !compressor.getPressureSwitchValue();
    }

    return returnValue;
  }
}
