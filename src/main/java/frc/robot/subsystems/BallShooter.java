// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import java.util.Map;
import java.util.function.DoubleSupplier;

import com.revrobotics.CANSparkMax;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.motorcontrol.Spark;
import edu.wpi.first.wpilibj.motorcontrol.Victor;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotConstants;
import frc.robot.commands.BallShooter.RunFeedMotor;
import frc.robot.commands.BallShooter.RunShooterMotor;
import frc.robot.commands.BallShooter.StopFeedMotor;
import frc.robot.commands.BallShooter.StopShooterMotor;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.RelativeEncoder;

public class BallShooter extends SubsystemBase {
  private CANSparkMax shooterMotor = new CANSparkMax(RobotConstants.BALL_SHOOTER_CAN_ID, MotorType.kBrushless); 
  private CANSparkMax feedMotor = new CANSparkMax(RobotConstants.VERTICAL_BELT_CAN_ID, MotorType.kBrushless);
  private RelativeEncoder shooterEncoder = shooterMotor.getEncoder(); 

  private DigitalInput ballPositionSwitch = new DigitalInput(RobotConstants.BALL_POSITION_SWITCH_DIGITAL_INPUT_PORT);
  
  private boolean shooterRunning = false;
  private boolean feedMotorRunning = false;
  private boolean useShooterSlider = true;
  private boolean useFeedSlider = true;

  // Dashboard stuff...
  private ShuffleboardTab shooterDashboardTab = Shuffleboard.getTab("Shooter");

  private ShuffleboardLayout  shooterMotorLayout  = shooterDashboardTab.getLayout("Shooter Motor", BuiltInLayouts.kList).withProperties(Map.of("Label Position", "HIDDEN")).withSize(2, 2).withPosition(0, 0);
  private ShuffleboardLayout  feedMotorLayout     = shooterDashboardTab.getLayout("Feed Motor", BuiltInLayouts.kList).withProperties(Map.of("Label Position", "HIDDEN")).withSize(2, 2).withPosition(2, 0);

  private NetworkTableEntry shooterSpeed;
  private NetworkTableEntry feedMotorSpeed;
  private NetworkTableEntry shooterRPMSpeed;

  public BallShooter() {
    //shooterEncoder = shooterMotor.getEncoder(); 
    initializeDashboard();
  }
  
  // Initialize/Create all of the dashboard stuff.
  private void initializeDashboard(){

    // Add widgets to control Shooter Motor
    shooterMotorLayout.add("Start", new RunShooterMotor(this, "Start Shooter"));
    shooterMotorLayout.add("Stop", new StopShooterMotor(this, "Stop Shooter"));
    shooterMotorLayout.addNumber("Motor Speed", this::getShooterSpeedSetting).withWidget(BuiltInWidgets.kNumberBar);
    shooterSpeed = shooterMotorLayout.add("Speed", 1).withWidget(BuiltInWidgets.kNumberSlider).withProperties(Map.of("min", -1, "max", 1)).getEntry();

    // Add widgets to control Feed Motor
    feedMotorLayout.add("Start", new RunFeedMotor(this, "Start Feeder"));
    feedMotorLayout.add("Stop", new StopFeedMotor(this, "Stop Feeder"));
    feedMotorLayout.addNumber("Motor Speed", this::getFeedMotorSpeedSetting).withWidget(BuiltInWidgets.kNumberBar);
    feedMotorSpeed = feedMotorLayout.add("Speed", RobotConstants.VERTICAL_BELT_PWM_VALUE).withWidget(BuiltInWidgets.kNumberSlider).withProperties(Map.of("min", -1, "max", 1)).getEntry();

    // Widget for ball postion switch
    shooterDashboardTab.addBoolean("Ball Position", this::isBallInPosition).withPosition(4, 0);
    shooterDashboardTab.addNumber("Shooter Speed", this::getShooterSpeed).withWidget(BuiltInWidgets.kDial).withProperties(Map.of("min", 0, "max", 8000)).withPosition(4, 1);
    shooterRPMSpeed = shooterDashboardTab.add("Shoot RPM", RobotConstants.BALL_SHOOTER_TARGET_SPEED).withWidget(BuiltInWidgets.kNumberSlider).withProperties(Map.of("min", 0, "max", 8000)).withPosition(5, 0).getEntry();

  }

  public void StartShooterMotor(double speed) {
    shooterSpeed.setDouble(speed);
    shooterMotor.set(speed); 
    shooterRunning = true;
  }

  public void StartShooterMotor(){
    this.StartShooterMotor(shooterSpeed.getDouble(1));
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    if (shooterRunning){
      shooterMotor.set(shooterSpeed.getDouble(1));
    }

    if (feedMotorRunning){
      feedMotor.set(feedMotorSpeed.getDouble(1));
    }
  }
  public void StopShooterMotor() {
    shooterMotor.set(0.0); 
    shooterRunning = false;
  }

  public void StartFeedMotor(double speed) {
    feedMotorSpeed.setDouble(speed);
    feedMotor.set(speed); 
    feedMotorRunning = true;
  }

  public void StartFeedMotor(){
    this.StartFeedMotor(feedMotorSpeed.getDouble(1));
  }

  public void StopFeedMotor() {
    feedMotor.set(0.0);
    feedMotorRunning = false; 
  }
  public double getShooterSpeed() {
    return shooterEncoder.getVelocity(); 
  }

  public double getShooterSpeedSetting(){
    return shooterMotor.get();
  }

  public double getFeedMotorSpeedSetting(){
    return feedMotor.get();
  }

  public boolean getBallPositionSwitch(){return ballPositionSwitch.get();}
  public boolean isBallInPosition(){return !ballPositionSwitch.get();}
  public double getShootRPMSpeed(){return shooterRPMSpeed.getDouble(1000);}
}
