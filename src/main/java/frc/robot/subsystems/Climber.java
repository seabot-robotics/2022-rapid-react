// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;


import frc.robot.RobotConstants;
import frc.robot.commands.Climber.RunLiftMotor;
import frc.robot.commands.Climber.StopLiftMotor;
import frc.robot.commands.Climber.TiltBack;
import frc.robot.commands.Climber.TiltForward;

import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import com.fasterxml.jackson.databind.deser.impl.ExternalTypeHandler.Builder;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class Climber extends SubsystemBase {

  public static final boolean tiltForward = false;
  // ** Motor and Encoders
  private CANSparkMax liftMotor = new CANSparkMax(RobotConstants.CLIMBER_LIFT_MOTOR_CAN_ID, MotorType.kBrushless);;
  private RelativeEncoder liftEncoder = liftMotor.getEncoder();

  private boolean liftMotorRunning = false;

  // ** Pnuematics
  private DoubleSolenoid tiltCylinder = new DoubleSolenoid(PneumaticsModuleType.CTREPCM, RobotConstants.CLIMBER_TILT_CYLINDER_FWD_CHANNEL, RobotConstants.CLIMBER_TILT_CYLINDER_REV_CHANNEL);
  private Compressor compressor = null;

  // ** Dashboard stuff
  private NetworkTableEntry liftMotorSpeed;
  
  private ShuffleboardTab climberDashboardTab = Shuffleboard.getTab("Climber");

  private ShuffleboardLayout liftMotorLayout = climberDashboardTab.getLayout("Lift Motor", BuiltInLayouts.kList).withProperties(Map.of("Label Position", "HIDDEN")).withSize(2, 3).withPosition(0, 0);
  private ShuffleboardLayout pneumaticsLayout = climberDashboardTab.getLayout("Pneumatics", BuiltInLayouts.kList).withProperties(Map.of("Label Position", "HIDDEN")).withSize(2, 2).withPosition(2, 0);

  /** Creates a new Climber. */
  public Climber() {
    tiltCylinder.set(Value.kForward);

    initizalizeDashboardTab();
  }

  public Climber(Compressor compressor){
    this();
    this.compressor = compressor;  
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run

    if (liftMotorRunning){
      liftMotor.set(liftMotorSpeed.getDouble(1));
    }
  }

  private void initizalizeDashboardTab(){
    // Lift Motor
    liftMotorSpeed = liftMotorLayout.add("Speed", 1).withWidget(BuiltInWidgets.kNumberSlider).withProperties(Map.of("min", -1, "max", 1)).getEntry();
    liftMotorLayout.add("StartLift", new RunLiftMotor(this, "Start Lift")).withWidget(BuiltInWidgets.kCommand);
    liftMotorLayout.add("Stop Lift", new StopLiftMotor(this, "Stop Lift")).withWidget(BuiltInWidgets.kCommand);
    liftMotorLayout.addNumber("Motor Value", this::getLiftMotorValue).withWidget(BuiltInWidgets.kNumberBar).withProperties(Map.of("min", -1, "max", 1));
    liftMotorLayout.addNumber("Encoder Postition", this::getEncoderPosition).withWidget(BuiltInWidgets.kTextView);

    // Pneumatics
    pneumaticsLayout.add("Tilt Forward", new TiltForward(this, "Tilt Forward")).withWidget(BuiltInWidgets.kCommand);
    pneumaticsLayout.add("Tilt Back", new TiltBack(this, "Tilt Back")).withWidget(BuiltInWidgets.kCommand);
    pneumaticsLayout.addBoolean("Extended", this::isTiltExtended);

    climberDashboardTab.addBoolean("Compressor",  this::isCompressorRunning).withPosition(2, 2);
    climberDashboardTab.addBoolean("Pressure Switch", this::getPressureSwitch).withPosition(3, 2);

  }

  // Speed should be value from 0.0 - 1.0... just to indicate speed... function will automatically negate to have the motor go the correct direction.
  public void lower(double speed){
    liftMotor.set(-speed);

  }

  // Speed should be value from 0.0 - 1.0... fuction will do appropriate math to make sure the motor goes the right direction...
  public void raise(double speed){
    liftMotor.set(speed);

  }

  public void tiltForward(){
    tiltCylinder.set(Value.kForward);
  }

  public void tiltBack(){
    tiltCylinder.set(Value.kReverse);
  }

  public void startLiftMotor() {
    startLiftMotor(liftMotorSpeed.getDouble(1));
  }

  public void startLiftMotor(double speed) {
    liftMotorSpeed.setDouble(speed);
    liftMotor.set(speed);
    liftMotorRunning = true;
  }

  public void stopLiftMotor() {
    liftMotor.set(0.0);
    liftMotorRunning = false;
  }
  
  public double getLiftMotorValue(){return liftMotor.get();}
  public boolean isTiltExtended(){return (tiltCylinder.get() == Value.kForward);}

  public boolean isCompressorRunning(){
    boolean returnValue = false;
    if (compressor != null){
      returnValue = compressor.enabled();
    }
    return returnValue;
  }

  public boolean getPressureSwitch(){
    boolean returnValue = false;
    if (compressor != null){
      returnValue = compressor.getPressureSwitchValue();
    }
    return returnValue;
  }

  public double getEncoderPosition(){
    return liftEncoder.getPosition(); // returns number of rotations
  }

}
