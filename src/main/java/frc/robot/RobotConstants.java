/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.util.sendable.Sendable;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.DriveTrain.MotorType;

/**
 * Class to contain constants used through out the robot.
 * 
 * NOTE: Trying to have central place for configuration.
 *       Please avoid declaring contants in other classes.
 */
public class RobotConstants {
    
    // Joystick ports
    public static final int DRIVER_JOYSTICK_PORT    = 0;
    public static final int OPERATOR_JOYSTICK_PORT  = 1;

    // ******* DRIVE TRAIN CONSTANTS *******
    public static final MotorType DEFAULT_DRIVETRAIN_CONTROLLER_TYPE = DriveTrain.MotorType.VictorSPX;
    // Encoder Constants
    public static final int DRIVE_TRAIN_LEFT_ENCODER_A  = 4;
    public static final int DRIVE_TRAIN_LEFT_ENCODER_B  = 5;
    public static final int DRIVE_TRAIN_RIGHT_ENCODER_A = 2;
    public static final int DRIVE_TRAIN_RIGHT_ENCODER_B = 3;  
    public static final double DRIVE_TRAIN_WHEEL_CIRCUMFERNCE       = 1.5707;  // Feet.  If Inches, 18.8495
    public static final double DRIVE_TRAIN_ENCODER_PULSES_PER_REV   = 1024;
    public static final boolean DRIVE_TRAIN_USE_LEFT_ENCODER = true;
    public static final boolean DRIVE_TRAIN_USE_RIGHT_ENCODER = false;    
    
    public static final int RESET_ENCODERS_JOYSTICK_BUTTON = 8;
    
    // PWM ports for Victors on DevBoard
    public static final int RIGHT_DEV_DRIVE_PWM = 1;
    public static final int LEFT_DEV_DRIVE_PWM  = 0;

    // CAN IDs for VictorSPX controllers
    public static final int LEFT_LEAD_MOTOR_CAN_ID      = 10;
    public static final int LEFT_FOLLOW_MOTOR_CAN_ID    = 11;
    public static final int RIGHT_LEAD_MOTOR_CAN_ID     = 13;
    public static final int RIGHT_FOLLOW_MOTOR_CAN_ID   = 14;

    // Gyro Drive PID Constants...
    public static final double kTurnRateToleranceDegPerS    = 1.0;
    public static final double kTurnToleranceDeg            = 0.1;
    public static final double kTurnP                       = 0.005;
    public static final double kTurnI                       = 0;
    public static final double kTurnD                       = 0;
    public static final boolean kGyroReversed               = false;

    // TurnToAngle Constants/Default values
    public static final int TURN_TO_ANGLE_RIGHT_BUTTON          = 7;
    public static final int TURN_TO_ANGLE_LEFT_BUTTON           = 6;
    public static final double TURN_TO_ANGLE_DEFAULT_AMOUNT     = 15;
    public static final double TURN_TO_ANGLE_DEFAULT_TIMEOUT    = 30;   
    
    // DriveHeading Constants/Default values
    public static final double DRIVE_HEADING_HEADING_TOLERANCE  = 0.1;
    public static final double DRIVE_HEADING_RATE_TOLERANCE     = 0.1;
    
    public static final int ZERO_GYRO_JOYSTICK_BUTTON           = 9;


    // Drive Stablization Constants...
    public static final int STABLIZE_DIRECTION_JOYSTICK_BUTTON = 1;    
    public static final double kStabilizationP = 1.0;
    public static final double kStabilizationI = 0.5;
    public static final double kStabilizationD = 0.0;

    // ******* Sonar and Blinkin Constants *******
    public static final int BLINKIN_PWM_CHANNEL     = 7;
    public static final double SONAR_GREEN_DIST     = 25;
    public static final double SONAR_YELLOW_DIST    = 10;
    public static final int SONAR_ANALOG_CHANNEL    = 3;
    
    // Motor ID's // 
    public static final int BALL_SHOOTER_CAN_ID     = 20;
    public static final int VERTICAL_BELT_CAN_ID    = 21;
    public static final int BALL_INTAKE_PWM_PORT    = 5;
    public static final int BALL_QUEUER_PWM_PORT    = 6; 
    
    // Motor Speeds // 
    public static final double BALL_SHOOTER_PWM_VALUE = 1;
    public static final double BALL_SHOOTER_TARGET_SPEED = 5400;
    public static final double VERTICAL_BELT_PWM_VALUE = 0.18;

    public static final double BALL_SHOOTER__SHOT_END_DELAY = 1.0;  // This is in seconds, delay for stopping the shooter after ball leaves queuing position
    public static final double BALL_SHOOTER_SHOOT_COMMAND_TIMEOUT = 7;

    public static final int BALL_COLLECTOR_CYLINDER_LEFT_CHANNEL1   = 0;
    public static final int BALL_COLLECTOR_CYLINDER_LEFT_CHANNEL2   = 1;
    public static final int BALL_COLLECTOR_CYLINDER_RIGHT_CHANNEL1  = 2;
    public static final int BALL_COLLECTOR_CYLINDER_RIGHT_CHANNEL2  = 3;

    public static final double BALL_COLLECTIOR_QUEUING_MOTOR_DEFAULT_SPEED = 0.3;

    public static final int BALL_POSITION_SWITCH_DIGITAL_INPUT_PORT = 6;

    

    // ******* Climber Constants *********
    public static final int CLIMBER_LIFT_MOTOR_CAN_ID = 22;
    public static final int CLIMBER_TILT_CYLINDER_FWD_CHANNEL = 4;
    public static final int CLIMBER_TILT_CYLINDER_REV_CHANNEL = 5;

    public static final double CLIMBER_RETRACT_BOTTOM_ENCODER_LIMIT = 2;
    public static final double CLIMBER_EXTEND_TILTFORWARD_UPPER_ENCODER_LIMIT = 140;
    public static final double CLIMBER_EXTEND_TILTBACK_UPPER_ENCODER_LIMIT = 170;

    // motor contants
    public static final double INTAKE_MOTOR_SPEED = 1;
    public static final double QUEUING_MOTOR_SPEED = 0.31;
    public static final double CLIMBER_EXTEND_LIFT_SPEED = 1;
    public static final double CLIMBER_RETRACT_LIFT_SPEED = -1;
    
    
    //Joystick Buttons - mainly operator joystick
    public static final int SHOOTING_BALLS_BUTTON = 1;
    public static final int TOGGLEING_INTAKE_BUTTON = 3;
    public static final int TOGGLEING_QUEING_MOTOR_BUTTON = 2;
    public static final int EXTEND_CLIMBER_BUTTON = 4;
    public static final int RETRACT_CLIMBER_BUTTON = 5;

    public static final double CLIMBER_OPERATOR_JOYSTICK_TILT_FORWARD_THRESHOLD = 0.5;
    public static final double CLIMBER_OPERATOR_JOYSTICK_TILT_BACKWARD_THRESHOLD = -0.5;
    
    public static final double AUTO_DRIVE_MOTOR_SPEED = .5;
}
