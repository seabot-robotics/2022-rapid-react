// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autonomous;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.BallShooter.ShootBall;
import frc.robot.commands.drivetrain.DriveDistanceSimple;
import frc.robot.commands.drivetrain.DriveHeading;
import frc.robot.commands.drivetrain.ResetEncoders;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.BallShooter;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class DriveSafety extends SequentialCommandGroup {
  /** Creates a new DriveSafety. */
  private static final double LEG1_DISTANCE = 6.66666; //80;

  public DriveSafety(double speed, DriveTrain driveTrain, BallShooter ballShooter) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new ShootBall(ballShooter),
      new ResetEncoders(driveTrain),
      //new DriveHeading(speed, 0.0, LEG1_DISTANCE, driveTrain)
      new DriveDistanceSimple(driveTrain, LEG1_DISTANCE, 0.3).withTimeout(5)
    );
  }
}
