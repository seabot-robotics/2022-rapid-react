// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autonomous;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.BallCollector.LowerBallCollector;
import frc.robot.commands.BallCollector.RunIntakeMotor;
import frc.robot.commands.BallCollector.RunQueuingMotor;
import frc.robot.commands.BallShooter.ShootBall;
import frc.robot.commands.drivetrain.DriveHeading; 
import frc.robot.commands.drivetrain.ResetEncoders; 
import frc.robot.commands.drivetrain.ZeroGyro;
import frc.robot.commands.drivetrain.TurnToAngle;
import frc.robot.subsystems.BallShooter;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.BallCollector;
import frc.robot.RobotConstants;



// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class DriveBasic extends SequentialCommandGroup {

  private static final double LEG1_DISTANCE = 80; //forward 80 inchs 
  private static final double LEG2_DISTANCE = 100;
  private static final double LEG3_DISTANCE = -127; //backwards 
  
  // These were added to get it to build...  :-)
  private static final double LEG4_DISTANCE = 0;
  private static final double TURN3_ANGLE = 0;
  // private static final double LEG4_DISTANCE = 2.0;
  // private static final double LEG5_DISTANCE = 1.0;
  
  private static final double TURN1_ANGLE = 135; //right 135 degrees
  private static final double TURN2_ANGLE = -135; //left 135 degrees 
  //private static final double TURN3_ANGLE = -55.0;
  // private static final double TURN4_ANGLE = 90.0;

  

  public DriveBasic(double speed, DriveTrain driveTrain, BallShooter ballShooter, PIDController turnPidController, BallCollector ballCollector) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      
      new ShootBall(ballShooter), 
      new ResetEncoders(driveTrain),
      new ZeroGyro(driveTrain),
      new LowerBallCollector(ballCollector, "lowerballcollector"),
      new RunIntakeMotor(ballCollector, RobotConstants.INTAKE_MOTOR_SPEED, "runintakemotor"),
      new DriveHeading(speed, 0.0, LEG1_DISTANCE, driveTrain),
      new RunQueuingMotor(ballCollector, RobotConstants.QUEUING_MOTOR_SPEED, "runqueuingmotor"),
      new TurnToAngle(TURN1_ANGLE, driveTrain, turnPidController),
      new ResetEncoders(driveTrain),
      new ZeroGyro (driveTrain),
      new DriveHeading(speed, 0.0, LEG2_DISTANCE, driveTrain),
      new TurnToAngle(TURN2_ANGLE, driveTrain, turnPidController),
      new ResetEncoders(driveTrain), 
      new ZeroGyro(driveTrain), 
      new DriveHeading(speed, 0.0, LEG3_DISTANCE, driveTrain),
      new ShootBall(ballShooter)
    );
  }
}
