// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autonomous;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveTrain;

public class DriveTime extends CommandBase {
  private DriveTrain driveTrain;
  private double speed = 0.0;
  private double startTime;
  private double time;
  private NetworkTableEntry speedSlider = null;
  private NetworkTableEntry timeSlider = null;

  /** Creates a new DriveTime. */
  public DriveTime(double speed, double time, DriveTrain driveTrain) {
    this.driveTrain = driveTrain;
    this.speed = speed;
    this.time = time;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveTrain);
    setName("Drive time and stop");
  }

  public DriveTime(NetworkTableEntry speedSlider, NetworkTableEntry timeSlider, DriveTrain driveTrain){
    this.speedSlider = speedSlider;
    this.timeSlider = timeSlider;
    this.speed = speedSlider.getDouble(0);
    this.time = timeSlider.getDouble(0) * 1000;
    this.driveTrain = driveTrain;
    addRequirements(driveTrain);
    setName("Drive with Sliders");
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    startTime = System.currentTimeMillis();
    if (speedSlider != null){
      speed = speedSlider.getDouble(0);
    }
    if (timeSlider != null){
      time = timeSlider.getDouble(0) * 1000;
    }
    driveTrain.resetEncoders();
    driveTrain.setArcadeValues(speed, 0, false);

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    driveTrain.setArcadeValues(speed, 0, false);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    driveTrain.setArcadeValues(0, 0);
    System.out.print("Encoder Distance: " + driveTrain.getEncoderDistance());
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    boolean returnValue = false;
    returnValue = System.currentTimeMillis() - startTime >= time;
    return returnValue;
  }
}
