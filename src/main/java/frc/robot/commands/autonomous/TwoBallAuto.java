// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autonomous;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.BallCollector.LowerBallCollector;
import frc.robot.commands.BallCollector.RunQueuingMotor;
import frc.robot.commands.BallShooter.QueueAndShoot;
import frc.robot.commands.drivetrain.DriveHeading;
import frc.robot.commands.drivetrain.ResetEncoders;
import frc.robot.subsystems.BallCollector;
import frc.robot.subsystems.BallShooter;
import frc.robot.subsystems.DriveTrain;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class TwoBallAuto extends SequentialCommandGroup {
  /** Creates a new TwoBallAuto. */
  public TwoBallAuto(double speed, double distance, DriveTrain driveTrain, PIDController turnPidController, BallShooter ballShooter, BallCollector ballCollector) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new ResetEncoders(driveTrain),
      new LowerBallCollector(ballCollector, "Lower Ball Collector"),
      new RunQueuingMotor(ballCollector, "Run Queuing Motor"),
      new DriveHeading(speed, 0, distance, driveTrain).withTimeout(4),
      new ResetEncoders(driveTrain),
      new DriveHeading(-speed, 0, -distance, driveTrain).withTimeout(4),
      new QueueAndShoot(ballShooter),
      new QueueAndShoot(ballShooter)
    );
  }
}
