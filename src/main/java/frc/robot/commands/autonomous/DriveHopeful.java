// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autonomous;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.math.controller.PIDController;
import frc.robot.commands.BallCollector.StartDownBallCollector;
import frc.robot.commands.BallCollector.StopQueuingMotor;
import frc.robot.commands.BallCollector.StopUpBallCollector;
import frc.robot.commands.BallCollector.RunQueuingMotor;
import frc.robot.commands.BallShooter.QueueAndShoot;
import frc.robot.commands.BallShooter.QueueBall;
import frc.robot.commands.BallShooter.ShootBall;
import frc.robot.commands.drivetrain.DriveDistanceSimple;
import frc.robot.commands.drivetrain.DriveHeading; 
import frc.robot.commands.drivetrain.ResetEncoders; 
import frc.robot.commands.drivetrain.ZeroGyro;
import frc.robot.commands.drivetrain.TurnToAngle;
import frc.robot.subsystems.BallShooter;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.BallCollector;
import frc.robot.RobotConstants;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class DriveHopeful extends SequentialCommandGroup {

  private static final double LEG1_DISTANCE = 3; 
  private static final double LEG2_DISTANCE = 8; 
  private static final double LEG3_DISTANCE = -4; 

  private static final double TURN1_ANGLE =  32;
  private static final double TURN2_ANGLE = 60; 
  private static final double TURN3_ANGLE = 0;

  /** Creates a new DriveHopeful. */
  public DriveHopeful(DriveTrain driveTrain, BallShooter ballShooter, PIDController turnPidController, BallCollector ballCollector) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new QueueAndShoot(ballShooter, "Queue and Shoot"),
      new StartDownBallCollector(ballCollector), 
      new ResetEncoders(driveTrain), 
      new DriveDistanceSimple(driveTrain, LEG1_DISTANCE, RobotConstants.AUTO_DRIVE_MOTOR_SPEED),
      new RunQueuingMotor(ballCollector, "Run Queuing Motor"),
      new QueueBall(ballShooter),
      new StopQueuingMotor(ballCollector, "Stop Queuing Motor"),
      new TurnToAngle(TURN1_ANGLE, driveTrain, turnPidController),
      new DriveDistanceSimple(driveTrain, LEG2_DISTANCE, RobotConstants.AUTO_DRIVE_MOTOR_SPEED),
      new StopUpBallCollector(ballCollector),
      new TurnToAngle(TURN2_ANGLE, driveTrain, turnPidController), 
      new DriveDistanceSimple(driveTrain, LEG3_DISTANCE, RobotConstants.AUTO_DRIVE_MOTOR_SPEED),
      new TurnToAngle(TURN3_ANGLE, driveTrain, turnPidController),  
      new ShootBall(ballShooter, "Shoot ball"),
      new QueueAndShoot(ballShooter, "Queue and Shoot") 

    );
  }
}
