// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autonomous;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.math.controller.PIDController;
import frc.robot.commands.BallCollector.StartDownBallCollector;
import frc.robot.commands.BallCollector.StopQueuingMotor;
import frc.robot.commands.BallCollector.StopUpBallCollector;
import frc.robot.commands.BallCollector.RunQueuingMotor;
import frc.robot.commands.BallShooter.QueueAndShoot;
import frc.robot.commands.BallShooter.QueueBall;
import frc.robot.commands.BallShooter.ShootBall;
import frc.robot.commands.drivetrain.DriveDistanceSimple;
import frc.robot.commands.drivetrain.DriveHeading; 
import frc.robot.commands.drivetrain.ResetEncoders; 
import frc.robot.commands.drivetrain.ZeroGyro;
import frc.robot.commands.drivetrain.TurnToAngle;
import frc.robot.subsystems.BallShooter;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.BallCollector;
import frc.robot.RobotConstants;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class DriveTwoBallLeft extends SequentialCommandGroup {

  private static final double LEG1_DISTANCE = 2; 
  private static final double LEG2_DISTANCE = -6.68; 

  /** Creates a new DriveTwoBallLeft. */
  public DriveTwoBallLeft(DriveTrain driveTrain, BallShooter ballShooter, PIDController turnPidController, BallCollector ballCollector) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new StartDownBallCollector(ballCollector),
      new RunQueuingMotor(ballCollector, 0.3, "runqueuingmotor"), 
      new ResetEncoders(driveTrain), 
      new ZeroGyro(driveTrain),
      new DriveDistanceSimple(driveTrain, LEG1_DISTANCE, 0.3).withTimeout(1), 
      new StopUpBallCollector(ballCollector), 
      new DriveDistanceSimple(driveTrain, LEG2_DISTANCE, 0.3).withTimeout(3),
      new ShootBall(ballShooter), 
      new QueueAndShoot(ballShooter)
    );
  }
}
