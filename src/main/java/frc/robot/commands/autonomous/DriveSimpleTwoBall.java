// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autonomous;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.BallCollector.LowerBallCollector;
import frc.robot.commands.BallCollector.RunQueuingMotor;
import frc.robot.commands.BallShooter.QueueAndShoot;
import frc.robot.commands.drivetrain.DriveDistanceSimple;
import frc.robot.commands.drivetrain.ResetEncoders;
import frc.robot.subsystems.BallCollector;
import frc.robot.subsystems.BallShooter;
import frc.robot.subsystems.DriveTrain;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class DriveSimpleTwoBall extends SequentialCommandGroup {
  /** Creates a new DriveSimpleTwoBall. */
  public DriveSimpleTwoBall(double speed, double distance, DriveTrain driveTrain, BallShooter ballShooter, BallCollector ballCollector) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new LowerBallCollector(ballCollector, "Lower BallCollect"),
      new RunQueuingMotor(ballCollector, "Run Queuing Motor"),
      new ResetEncoders(driveTrain),
      new DriveDistanceSimple(driveTrain, distance, speed).withTimeout(4),
      new ResetEncoders(driveTrain),
      new DriveDistanceSimple(driveTrain, -distance, -speed).withTimeout(4),
      new QueueAndShoot(ballShooter),
      new QueueAndShoot(ballShooter)
    );
  }
}
