// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Climber;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Climber;

public class RunLiftMotor extends CommandBase {
  private Climber climber;
  private double speed = 0.0;
  
  /** Creates a new RunLiftMotor. */
  public RunLiftMotor(Climber climber, double speed, String commandName) {
    this.climber = climber;
    this.speed = speed;
    setName(commandName);

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(climber);
  }

  public RunLiftMotor(Climber climber, String commandName){
    this(climber, 0.0, commandName);
  }

  public RunLiftMotor(Climber climber, double speed){
    this(climber, speed, "RunClimberLiftMotor");
  }

  public RunLiftMotor(Climber climber){
    this(climber, 0.0, "RunClimberLiftMotor");
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    if (speed == 0.0){
      climber.startLiftMotor();
    }
    else{
      climber.startLiftMotor(speed);
    }
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return true;
  }
}
