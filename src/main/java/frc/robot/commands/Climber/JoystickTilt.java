// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Climber;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.RobotConstants;
import frc.robot.subsystems.Climber;

public class JoystickTilt extends CommandBase {
  private Climber climber;
  private Joystick joystick;
  /** Creates a new JoystickTilt. */
  public JoystickTilt(Climber climber, Joystick joystick) {
    this.climber = climber;
    this.joystick = joystick;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(climber);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double yAxisValue = -joystick.getY();
    if (yAxisValue > RobotConstants.CLIMBER_OPERATOR_JOYSTICK_TILT_FORWARD_THRESHOLD){
      climber.tiltForward();
    }
    else if (yAxisValue < RobotConstants.CLIMBER_OPERATOR_JOYSTICK_TILT_BACKWARD_THRESHOLD){
      climber.tiltBack();
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
