// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Climber;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.RobotConstants;
import frc.robot.subsystems.Climber;

public class RetractClimber extends CommandBase {
  private Climber climber;
  /** Creates a new RetractClimber. */
  public RetractClimber(Climber climber) {
    this.climber = climber; 
    
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(climber);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    double encoderPosition = climber.getEncoderPosition(); 
    if (encoderPosition > RobotConstants.CLIMBER_RETRACT_BOTTOM_ENCODER_LIMIT){
      climber.startLiftMotor(RobotConstants.CLIMBER_RETRACT_LIFT_SPEED);
    }
    else { 
      climber.stopLiftMotor();
    }
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double encoderPosition = climber.getEncoderPosition(); 
    if (encoderPosition > RobotConstants.CLIMBER_RETRACT_BOTTOM_ENCODER_LIMIT)
    {
      climber.startLiftMotor(RobotConstants.CLIMBER_RETRACT_LIFT_SPEED);
    }
    else { 
      climber.stopLiftMotor();
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    climber.stopLiftMotor();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
