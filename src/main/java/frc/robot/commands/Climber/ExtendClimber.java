// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Climber;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.RobotConstants;
import frc.robot.RobotContainer;
import frc.robot.subsystems.Climber;

public class ExtendClimber extends CommandBase {
  private Climber climber; 
  private double upperLimit; 
  /** Creates a new ExtendClimber. */
  public ExtendClimber(Climber climber) {
    this.climber = climber; 
    addRequirements(climber);
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    double encoderPosition = climber.getEncoderPosition();
    boolean tiltedForward = climber.isTiltExtended();  
    if (tiltedForward &&(encoderPosition > RobotConstants.CLIMBER_EXTEND_TILTFORWARD_UPPER_ENCODER_LIMIT))
    {
      climber.stopLiftMotor(); 
    }
      else if (!tiltedForward &&(encoderPosition > RobotConstants.CLIMBER_EXTEND_TILTBACK_UPPER_ENCODER_LIMIT)) {
      climber.stopLiftMotor();
      }
    else { 
      climber.startLiftMotor(RobotConstants.CLIMBER_EXTEND_LIFT_SPEED);
    }
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double encoderPosition = climber.getEncoderPosition(); 
    boolean tiltedForward = climber.isTiltExtended(); 

    if (tiltedForward && (encoderPosition > RobotConstants.CLIMBER_EXTEND_TILTBACK_UPPER_ENCODER_LIMIT)) 
    {
      climber.stopLiftMotor();
    }
    else if (!tiltedForward &&(encoderPosition > RobotConstants.CLIMBER_EXTEND_TILTBACK_UPPER_ENCODER_LIMIT)){
      climber.stopLiftMotor();
    }
    else{
      climber.startLiftMotor(RobotConstants.CLIMBER_EXTEND_LIFT_SPEED);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    climber.stopLiftMotor();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
