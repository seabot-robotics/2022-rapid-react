// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.BallCollector;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.BallCollector;

public class RaiseBallCollector extends CommandBase {
  private BallCollector ballCollector;

  /** Creates a new RaiseBallCollector. */
  public RaiseBallCollector(BallCollector ballCollector, String commandName) {
    this.ballCollector = ballCollector;
    setName(commandName);

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(ballCollector);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    ballCollector.raiseCollector();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return true;
  }
}
