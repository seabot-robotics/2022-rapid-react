// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.BallCollector;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.BallCollector;

public class RunQueuingMotor extends CommandBase {
  private BallCollector ballCollector;
  private double speed;

  /** Creates a new RunQueuingMotor. */
  public RunQueuingMotor(BallCollector ballCollector, double speed, String commandName) {
    this.ballCollector = ballCollector;
    this.speed = speed;
    setName(commandName);

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(ballCollector);
  }

  public RunQueuingMotor (BallCollector ballCollector, String commandName){
    this(ballCollector, 0.0, commandName);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    if (speed == 0.0){
      this.ballCollector.startQueuingMotor();
    }
    else{
      this.ballCollector.startQueuingMotor(speed);
    }
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return true;
  }
}
