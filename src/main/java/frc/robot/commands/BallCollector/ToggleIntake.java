// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.BallCollector;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.BallCollector;

public class ToggleIntake extends CommandBase {
  private BallCollector collector; 

  /** Creates a new ToggleIntake. */
  public ToggleIntake(BallCollector collector) {
    this.collector = collector; 
    
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(collector);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    if (collector.isIntakeMotorRunning()){
      collector.raiseCollector();
      collector.stopIntakeMotor(); 
    } 
    else {
      collector.lowerCollector();
      collector.startIntakeMotor();
    }
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return true;
  }
}
