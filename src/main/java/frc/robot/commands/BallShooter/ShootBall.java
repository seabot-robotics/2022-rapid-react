// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.BallShooter;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.RobotConstants;
import frc.robot.subsystems.BallShooter;

public class ShootBall extends CommandBase {
  private boolean ballQueued = false;
  private Timer timer = new Timer();
  private double shootRPM = 0.0;
  private BallShooter ballShooter;


  /** Creates a new ShootBall. */
  public ShootBall(BallShooter ballShooter, String commandName) {
   this.ballShooter = ballShooter; 
   this.setName(commandName);

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(ballShooter);
  }

  public ShootBall(BallShooter ballShooter) {
    this(ballShooter, "Shoot Ball Command");
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    ballQueued = ballShooter.isBallInPosition();
    timer.stop();
    timer.reset();
    ballShooter.StartShooterMotor();
    shootRPM = ballShooter.getShootRPMSpeed();
    
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    // Need to figure out the speed that produces the result we want.
    if (ballShooter.getShooterSpeed() >= shootRPM && ballQueued) {
      ballShooter.StartFeedMotor();
    }

    if (!ballQueued && ballShooter.isBallInPosition()){ // Ball was not queued... now it is...  This SHOULD NOT HAPPEN, as ball should be queued before being called.
      ballQueued = true;
    } 

    if (ballQueued && !ballShooter.isBallInPosition()){ // ball was queued, now it is gone...
      ballShooter.StopFeedMotor();
      timer.start();
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    ballShooter.StopFeedMotor();
    ballShooter.StopShooterMotor();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return (timer.hasElapsed(RobotConstants.BALL_SHOOTER__SHOT_END_DELAY)); // End after the ball was shot...
  }
}
