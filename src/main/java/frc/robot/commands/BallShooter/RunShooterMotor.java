// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.BallShooter;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.BallShooter;

public class RunShooterMotor extends CommandBase {
  // Members
  private BallShooter ballShooter;
  private double      speed = 0.0;

  /** Creates a new RunShooterMotor. */
  public RunShooterMotor(BallShooter ballShooter, double speed, String commandName) {
    this.ballShooter = ballShooter;
    this.speed = speed;
    setName(commandName);

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(ballShooter);
  }

  public RunShooterMotor(BallShooter ballShooter){
    this(ballShooter, 0.0, "Run Shooter Motor");
  }

  public RunShooterMotor(BallShooter ballShooter, String commandName){
    this(ballShooter, 0.0, commandName);
  }

  public RunShooterMotor(BallShooter ballShooter, double speed){
    this(ballShooter, speed, "Run Shooter Motor");
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    if (speed == 0.0){ // no speed passed....
      ballShooter.StartShooterMotor();
    }
    else{
      ballShooter.StartShooterMotor(speed);
    }
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return true;
  }
}
