// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.BallShooter;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.BallShooter;

public class StopShooterMotor extends CommandBase {
  BallShooter ballShooter;

  /** Creates a new StopShooterMotor. */
  public StopShooterMotor(BallShooter ballShooter, String commandName) {
    this.ballShooter = ballShooter;
    this.setName(commandName);

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(ballShooter);
  }

  public StopShooterMotor(BallShooter ballShooter){
    this(ballShooter, "Stop Shooter Motor");
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    ballShooter.StopShooterMotor();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    ballShooter.StopShooterMotor();
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    ballShooter.StopShooterMotor();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return true;
  }
}
