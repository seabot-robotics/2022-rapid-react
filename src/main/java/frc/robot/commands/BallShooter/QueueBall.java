// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.BallShooter;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.commands.Climber.RunLiftMotor;
import frc.robot.subsystems.BallShooter;
import frc.robot.subsystems.Climber;

public class QueueBall extends CommandBase {
  private BallShooter ballShooter;
  private boolean finished = false;

  /** Creates a new QueueBall. */
  public QueueBall(BallShooter ballShooter) {
    this.ballShooter = ballShooter;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(ballShooter);

  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() { // No ball in position, run the feed motor
    if (!ballShooter.isBallInPosition()){
        ballShooter.StartFeedMotor();
        finished = false;
    }
    else{ // There is a ball in position - don't need to run the motor, and signal it is finished
      ballShooter.StopFeedMotor();
      finished = true;
    }
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (ballShooter.getBallPositionSwitch() == false){ // ball in position, stop feed motor
      ballShooter.StopFeedMotor();
      finished = true;
    }
    // else, ball is not in position so let the feed motor continue to run
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    ballShooter.StopFeedMotor();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return finished;
  }
}
