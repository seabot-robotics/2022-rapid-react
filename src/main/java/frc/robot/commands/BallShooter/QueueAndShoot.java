// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.BallShooter;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.BallShooter;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class QueueAndShoot extends SequentialCommandGroup {
  /** Creates a new QueueAndShoot. */
  public QueueAndShoot(BallShooter ballShooter) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new RunShooterMotor(ballShooter), // start the shooter motor so it can get up to speed and be ready to shoot ball
      new QueueBall(ballShooter),       // get ball to the queue postion
      new ShootBall(ballShooter)        // move ball from queue postion and shoot!
    );
  }

  public QueueAndShoot(BallShooter ballShooter, String string) {
  }
}
