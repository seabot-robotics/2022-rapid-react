// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.BallShooter;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.BallShooter;

public class RunFeedMotor extends CommandBase {
  BallShooter ballShooter;
  double speed = 0.0;

  /** Creates a new RunFeedMotor. */
  public RunFeedMotor(BallShooter ballShooter, double speed, String commandName) {
    this.ballShooter = ballShooter;
    this.speed = speed;
    setName(commandName);

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(ballShooter);
  }

  public RunFeedMotor(BallShooter ballShooter, String commandName){
    this(ballShooter, 0.0, commandName);
  }

  public RunFeedMotor(BallShooter ballShooter){
    this(ballShooter, 0.0, "Run Feed Motor");
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    if (speed == 0.0){
      ballShooter.StartFeedMotor();
    }
    else{
      ballShooter.StartFeedMotor(speed);
    }
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return true;
  }
}
