// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.BallShooter;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.BallShooter;

public class StopFeedMotor extends CommandBase {

  private BallShooter ballShooter;

  /** Creates a new StopFeedMotor. */
  public StopFeedMotor(BallShooter ballShooter, String commandName) {
    this.ballShooter = ballShooter;
    this.setName(commandName);

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(ballShooter);
  }

  public StopFeedMotor(BallShooter ballShooter){
    this(ballShooter, "Stop Feed Motor");
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    ballShooter.StopFeedMotor();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {ballShooter.StopFeedMotor();}

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return true;
  }
}
