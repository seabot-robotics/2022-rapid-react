// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Blinkin;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.RobotConstants;
import frc.robot.SonarModule;
import frc.robot.subsystems.Blinkin;

public class BlinkinSonar extends CommandBase {
  Blinkin blinkin;
  SonarModule sonarModule;
  /** Creates a new BlinkinSonar. */
  public BlinkinSonar(Blinkin blinkin, SonarModule sonarModule) {
    this.blinkin = blinkin;
    this.sonarModule = sonarModule;
    this.setName("Sonar Blinkin");

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(blinkin);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    blinkin.setPrimaryColor(Blinkin.COLOR_GREEN);
    blinkin.setAlternateColor(Blinkin.COLOR_BLACK);
    blinkin.setBlink(true);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    blinkin.setBlink(true);
    double dist = sonarModule.getInchesValue();
    if ( dist > RobotConstants.SONAR_GREEN_DIST){
      blinkin.setPrimaryColor(Blinkin.COLOR_GREEN);
    } else if (dist > RobotConstants.SONAR_YELLOW_DIST){
      blinkin.setPrimaryColor(Blinkin.COLOR_YELLOW);
    } else {
      blinkin.setPrimaryColor(Blinkin.COLOR_RED);
    }
    blinkin.setInterval((int) sonarModule.getInchesValue());
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
