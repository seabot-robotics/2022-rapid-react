// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Blinkin;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Blinkin;

public class BlinkinCompressor extends CommandBase {
  Blinkin blinkin;
  Compressor compressor;

  /** Creates a new BlinkinCompressor. */
  public BlinkinCompressor(Blinkin blinkin, Compressor compressor, String commandName) {
    this.blinkin = blinkin;
    this.compressor = compressor;
    setName(commandName);

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(blinkin);
  }

  public BlinkinCompressor(Blinkin blinkin, Compressor compressor){
    this(blinkin, compressor, "Compressor Blinkin");
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    blinkin.setPrimaryColor(Blinkin.COLOR_RED);
    blinkin.setAlternateColor(Blinkin.COLOR_BLACK);
    blinkin.setBlink(true);
    blinkin.setInterval(50);

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (this.compressor.enabled()){
      blinkin.setPrimaryColor(Blinkin.COLOR_RED);
      blinkin.setBlink(true);
    }
    else{
      blinkin.setPrimaryColor(Blinkin.COLOR_GREEN);
      blinkin.setBlink(false);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
