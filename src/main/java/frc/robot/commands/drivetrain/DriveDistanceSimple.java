// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.drivetrain;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveTrain;

public class DriveDistanceSimple extends CommandBase {
  private DriveTrain driveTrain;
  private Double distance = 0.0;
  private Double speed;
  /** Creates a new DriveDistanceSimple. */
  public DriveDistanceSimple(DriveTrain driveTrain, double distance, double speed) {
    this.driveTrain = driveTrain;
    this.distance = distance;
    this.speed = speed;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveTrain);

  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    driveTrain.resetEncoders();
    if (driveTrain.getEncoderDistance() < distance)
    {
      driveTrain.setArcadeValues(speed, 0, false); // Need to set squaredInputs to false so motors get set to the correct value
    }
    else{
      driveTrain.setArcadeValues(0, 0);
    }
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (driveTrain.getEncoderDistance() < distance)
    {
      driveTrain.setArcadeValues(speed, 0, false); // Need to set squaredInputs to false so motors get set to the correct value
    }
    else{
      driveTrain.setArcadeValues(0, 0);
    }
   
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    driveTrain.setArcadeValues(0, 0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return (driveTrain.getEncoderDistance()>= distance);
  }
}
