// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.drivetrain;

import edu.wpi.first.math.controller.PIDController;
import frc.robot.RobotConstants;
import frc.robot.subsystems.DriveTrain;
import edu.wpi.first.wpilibj2.command.PIDCommand;

// TODO: Command currently does Absolute Heading... Would like to enable option for referential turn... i.e. current heading +/- value...  resettng to zero before turn would be acceptable

/** A command that will turn the robot to the specified angle. */
public class TurnToAngle extends PIDCommand {
  private DriveTrain driveTrain;
  /**
   * Turns to robot to the specified angle.
   *
   * @param targetAngleDegrees The angle to turn to
   * @param drive The drive subsystem to use
   */
  public TurnToAngle(double targetAngleDegrees, DriveTrain drive, PIDController pidController) {
    super(
        pidController,
        // Close loop on heading
        drive::getHeading,
        // Set reference to target
        targetAngleDegrees,
        // Pipe output to turn robot
        output -> drive.setArcadeValues(0, output, false),
        // Require the drive
        drive);

    setName("Turn " + targetAngleDegrees + " degrees");

    // Set the controller to be continuous (because it is an angle controller)
    getController().enableContinuousInput(-180, 180);
    // Set the controller tolerance - the delta tolerance ensures the robot is stationary at the
    // setpoint before it is considered as having reached the reference
    getController()
        .setTolerance(RobotConstants.kTurnRateToleranceDegPerS);
    driveTrain = drive;
  }

  public TurnToAngle(double targetAngleDegrees, DriveTrain drive){
    this(targetAngleDegrees, drive, new PIDController(RobotConstants.kTurnP, RobotConstants.kTurnI, RobotConstants.kTurnD));
  }

  @Override
  public void initialize() {
    // TODO Auto-generated method stub
    super.initialize();
    driveTrain.zeroHeading();
  }

  /** 
   * Method that is called by the scheduler to see if the command is finished.
   * 
   * @return boolean - True when command is complete, False for when command is NOT complete
   */
  @Override
  public boolean isFinished() {
    // End when the controller is at the reference.
    return getController().atSetpoint();
  }
}
