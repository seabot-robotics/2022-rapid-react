/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.drivetrain;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveTrain;

/**
 * Class/Command to enable driving the robot in Arcade Mode with a joystick or other means
 * 
 */
public class ArcadeDrive extends CommandBase {
  
  // ***** MEMBER VARIABLES *****
  private final DriveTrain driveTrain;
  private final Joystick joystick;
  
  /**
   * Creates a new ArcadeDrive.
   */
  public ArcadeDrive(DriveTrain driveTrain, Joystick joystick) {
    this.driveTrain = driveTrain;
    this.joystick   = joystick;
    
    addRequirements(driveTrain);  // Command requires the DriveTrain subsystem to work
  }

  /**
   * Method for initializing the command
   * 
   * NOTE: Called when the command is initially scheduled.  This is called only once
   */
  @Override
  public void initialize() {
    // Don't have any initialization to do for this command
    driveTrain.setMaxOutput();
  }

  /**
   * Called every time the scheduler runs while the command is scheduled.
   * 
   * NOTE: This will be called multiple times, about every 50 ms. 
   */ 
  @Override
  public void execute() {
    driveTrain.setArcadeValues(-joystick.getY(), joystick.getX());
  }

  /**
   * Called once the command ends or is interrupted.
   * 
   * @param interrupted - Indicates if the command was interrupted
   * 
   */
  @Override
  public void end(boolean interrupted) {
    driveTrain.setArcadeValues(0, 0);
  }

  /**
   * Method that is called by the scheduler to see if the command is done.  
   * 
   * @return boolean - True indicates command is done, False command is NOT done.
   */
  @Override
  public boolean isFinished() {
    // This command should never end...
    return false;
  }
}

