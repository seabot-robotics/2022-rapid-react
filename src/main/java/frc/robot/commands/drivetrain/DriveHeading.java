// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.drivetrain;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj2.command.PIDCommand;
import frc.robot.RobotConstants;
import frc.robot.subsystems.DriveTrain;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class DriveHeading extends PIDCommand {
  // ***** Members *****
  private DriveTrain driveTrain;
  private double distance = 0.0;

  /** Creates a new DriveHeading. */
  public DriveHeading(double speed, double heading, double distance, DriveTrain driveTrain) {
    super(
        // The controller that the command will use
        new PIDController(RobotConstants.kStabilizationP, 
                          RobotConstants.kStabilizationI, 
                          RobotConstants.kStabilizationP),
        // This should return the measurement
        driveTrain::getHeading,
        // This should return the setpoint (can also be a constant)
        heading,
        // This uses the output
        output -> driveTrain.setArcadeValues(speed, output, false),
        // requires this subsystem...
        driveTrain);

    // Configure additional PID options by calling `getController` here.
    getController().enableContinuousInput(-180, 180);
    getController().setTolerance(RobotConstants.DRIVE_HEADING_HEADING_TOLERANCE, RobotConstants.DRIVE_HEADING_RATE_TOLERANCE);
    this.driveTrain = driveTrain;
    this.distance = distance;
    setName("DriveHeading");
  }

  public DriveHeading (double speed, double heading, DriveTrain driveTrain){
    this(speed, heading, 0.0, driveTrain);
  }

  @Override
  public void end(boolean interrupted) {
    super.end(interrupted);
    driveTrain.setArcadeValues(0, 0);
  }

  // Returns true when the command should end.  If the distance to travel is 0.0, it will continue until canceled or interupted. 
  @Override
  public boolean isFinished() {
    boolean returnValue = false;

    if (distance > 0 && driveTrain.getEncoderDistance() >= distance){
      returnValue = true;
    }

    return returnValue;
  }
}
