// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.drivetrain;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveTrain;

public class DriveSimpleArcToHeading extends CommandBase {
  // **** Members
  private double exitHeading;
  private double forwardSpeed;
  private double turnSpeed;
  private DriveTrain driveTrain;
  private boolean useDashboardValues = false;

  /** Creates a new DriveSimpleArcToHeading. */
  public DriveSimpleArcToHeading(double exitHeading, double forwardSpeed, double turnSpeed, DriveTrain driveTrain) {
    this.exitHeading = exitHeading;
    this.forwardSpeed = forwardSpeed;
    this.turnSpeed = turnSpeed;
    this.driveTrain = driveTrain;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveTrain);
  }

  public DriveSimpleArcToHeading (DriveTrain driveTrain){
    addRequirements(driveTrain);
    this.driveTrain = driveTrain;
    useDashboardValues = true;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    if (useDashboardValues){
      forwardSpeed = driveTrain.getArcForwardSpeed();
      turnSpeed = driveTrain.getArcTurnSpeed();
      exitHeading = driveTrain.getArcDegrees();
    }
    driveTrain.zeroHeading();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    driveTrain.setArcadeValues(forwardSpeed, turnSpeed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    driveTrain.setArcadeValues(0, 0); // Stop the drive train
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    boolean returnValue = false;

    if (exitHeading >= 0){
      returnValue = driveTrain.getHeading() >= exitHeading - 1;  // adding some tollerance - Could get this from the dashboard
    }
    else if (exitHeading < 0){
      returnValue = driveTrain.getHeading() <= exitHeading + 1;
    }

    return returnValue;
  }
}
