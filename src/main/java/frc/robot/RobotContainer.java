// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.util.Map;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.commands.BallCollector.StartDownBallCollector;
import frc.robot.commands.BallCollector.StopUpBallCollector;
import frc.robot.commands.BallCollector.ToggleIntake;
import frc.robot.commands.BallCollector.ToggleQueingMotor;
import frc.robot.commands.BallShooter.DefaultBallShooterCommand;
import frc.robot.commands.BallShooter.QueueAndShoot;
import frc.robot.commands.BallShooter.QueueBall;
import frc.robot.commands.BallShooter.ShootBall;
import frc.robot.commands.Climber.ExtendClimber;
import frc.robot.commands.Climber.JoystickTilt;
import frc.robot.commands.Climber.RetractClimber;
import frc.robot.commands.Climber.RunLiftMotor;
import frc.robot.commands.autonomous.DriveBasic;
import frc.robot.commands.autonomous.DriveForwardAndBack;
import frc.robot.commands.autonomous.DriveHopeful;
import frc.robot.commands.autonomous.DriveSafety;
import frc.robot.commands.autonomous.DriveSimpleTwoBall;
import frc.robot.commands.autonomous.DriveThreeBallRight;
import frc.robot.commands.autonomous.DriveTwoBallLeft;
import frc.robot.commands.autonomous.SimpleDriveForwardAndBack;
import frc.robot.commands.autonomous.SimpleDriveTurnAndDrive;
import frc.robot.commands.autonomous.TwoBallAuto;
import frc.robot.commands.drivetrain.ArcadeDrive;
import frc.robot.commands.drivetrain.DriveDistanceSimple;
import frc.robot.commands.drivetrain.DriveHeading;
import frc.robot.subsystems.BallCollector;
import frc.robot.subsystems.BallShooter;
import frc.robot.subsystems.Climber;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.Blinkin;
import frc.robot.commands.Blinkin.BlinkinCompressor;
import frc.robot.commands.Blinkin.BlinkinSonar;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.FunctionalCommand;
import edu.wpi.first.wpilibj2.command.PIDCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;

public class RobotContainer{

    private SendableChooser<Command> autonomousChooser = new SendableChooser<>();

    private Joystick driverJoystick = new Joystick (0);
    private Joystick operatorJoystick = new Joystick (1);
    private final DriveTrain driveTrain = new DriveTrain (RobotConstants.DRIVE_TRAIN_USE_LEFT_ENCODER,
                                                          RobotConstants.DRIVE_TRAIN_USE_RIGHT_ENCODER, 
                                                          DriveTrain.MotorType.VictorSPX);

    private PIDController turnPidController = new PIDController (RobotConstants.kTurnP, RobotConstants.kTurnI, RobotConstants.kTurnD);
                                                          
    private final ArcadeDrive arcadeJoystickDrive = new ArcadeDrive(driveTrain, driverJoystick);
    
    //private ShuffleboardTab smartDashboardTab =  Shuffleboard.getTab("Auto");

    // Pnuematics stuff
    private Compressor compressor = new Compressor(PneumaticsModuleType.CTREPCM);

    // Blinkin & Sonar
    private Blinkin blinkin = new Blinkin();
    public SonarModule sonarModule = new SonarModule(RobotConstants.SONAR_ANALOG_CHANNEL);

    public Command blinkinCompressor = new BlinkinCompressor (blinkin, compressor);
    public Command blinkinSonar = new BlinkinSonar(blinkin, sonarModule);


    private final BallShooter ballShooter = new BallShooter();
    private final BallCollector ballCollector = new BallCollector(compressor);
    private final Climber climber = new Climber(compressor);
    private final JoystickTilt joystickClimberTilt = new JoystickTilt(climber, operatorJoystick);

    public RobotContainer(){
        
        configureButtonBindings(); 
        driveTrain.setDefaultCommand(arcadeJoystickDrive);
        climber.setDefaultCommand(joystickClimberTilt);
        blinkin.setDefaultCommand(blinkinCompressor);
        ballShooter.setDefaultCommand(new DefaultBallShooterCommand(ballShooter));

        autonomousChooser.setDefaultOption("Drive Basic", new DriveBasic(0.25, driveTrain, ballShooter, turnPidController, ballCollector));
        autonomousChooser.addOption("Drive Three Ball", new DriveThreeBallRight(driveTrain, ballShooter, turnPidController, ballCollector));
        autonomousChooser.addOption("Drive Two Ball", new DriveTwoBallLeft(driveTrain, ballShooter, turnPidController, ballCollector));
        autonomousChooser.addOption("One Ball Auto (was: Drive Safety)", new DriveSafety(0.5, driveTrain, ballShooter)); 
        autonomousChooser.addOption("Drive Hopeful", new DriveHopeful(driveTrain, ballShooter, turnPidController, ballCollector));
        autonomousChooser.addOption("TEST - Drive Distance Simple: 1.5 ft", new DriveDistanceSimple(driveTrain, 1.5, 0.3));
        autonomousChooser.addOption("TEST - Drive Distance Simple: 5 ft", new DriveDistanceSimple(driveTrain, 5, 0.3));
        autonomousChooser.addOption("TEST - Drive Distance Simple: 10 ft", new DriveDistanceSimple(driveTrain, 10, 0.3));
        autonomousChooser.addOption("TEST - Drive Distance Simple, Forward/Back: 5 ft", new SimpleDriveForwardAndBack(0.3, 5, driveTrain));
        autonomousChooser.addOption("TEST - Queue and Shoot", new QueueAndShoot(ballShooter));
        autonomousChooser.addOption("TEST - ShootBall", new ShootBall(ballShooter, "Test ShootBall"));
        autonomousChooser.addOption("TEST - Drive Straight(heading) - 10 ft", new DriveHeading(0.3, 0, 10, driveTrain));
        autonomousChooser.addOption("TEST - Drive Straight(heading) - 5 ft", new DriveHeading(0.3, 0, 5, driveTrain));
        autonomousChooser.addOption("TEST - Drive, Turn, Drive: 5 ft, 45 degrees, 5ft", new SimpleDriveTurnAndDrive(0.3, 5, 45, driveTrain, turnPidController));
        autonomousChooser.addOption("TEST - Drive, Turn, Drive: 5 ft, 90 degrees, 5ft", new SimpleDriveTurnAndDrive(0.3, 5, 90, driveTrain, turnPidController));
        autonomousChooser.addOption("TEST - Drive Forward and Back", new DriveForwardAndBack(0.3, 5, driveTrain));
        autonomousChooser.addOption("TEST - Drive Simple Two Ball", new DriveSimpleTwoBall(0.3, 5, driveTrain, ballShooter, ballCollector));
        autonomousChooser.addOption("TEST - Two Ball Auto", new TwoBallAuto(0.3, 5, driveTrain, turnPidController, ballShooter, ballCollector));

        SmartDashboard.putData("Auto Modes", autonomousChooser);
         
       }
     
    public Command getAutonomousCommand() {
        // An ExampleCommand will run in autonomous
        return autonomousChooser.getSelected();
    }

    public boolean getPressureSwitch(){
        return !compressor.getPressureSwitchValue();
    }

    public boolean isCompressorRunning(){
        return compressor.enabled();
    }
    private void configureButtonBindings() {

        new JoystickButton(operatorJoystick, RobotConstants.SHOOTING_BALLS_BUTTON)
        .whenPressed(new QueueAndShoot(ballShooter).withTimeout(RobotConstants.BALL_SHOOTER_SHOOT_COMMAND_TIMEOUT)); 

        new JoystickButton(driverJoystick, RobotConstants.TOGGLEING_INTAKE_BUTTON)
        .whenPressed(new ToggleIntake(ballCollector)); 

        new JoystickButton(operatorJoystick, RobotConstants.TOGGLEING_QUEING_MOTOR_BUTTON)
        .whenPressed(new ToggleQueingMotor(ballCollector)); 

        new JoystickButton(operatorJoystick, RobotConstants.EXTEND_CLIMBER_BUTTON)
        .whenHeld(new ExtendClimber(climber)); 

        new JoystickButton(operatorJoystick, RobotConstants.RETRACT_CLIMBER_BUTTON)
        .whenHeld(new RetractClimber(climber));

        new JoystickButton(operatorJoystick, 3)
        .whenPressed(new QueueBall(ballShooter));

        new JoystickButton(driverJoystick, 2)
        .whenPressed(new QueueAndShoot(ballShooter)); 

    




    }

}