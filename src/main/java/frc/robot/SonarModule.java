// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/** Add your docs here. */
public class SonarModule {
    // constants
    public static final double INCHES_CONVERSION_FACTOR     = 0.0492;  // mV per inch
    public static final double FEET_CONVERSION_FACTOR       = 12;  // inches per foot
    private double voltageScaleFactor = 5/RobotController.getVoltage5V();

    // Member variables
    AnalogInput sonarAnalogInput;

    public SonarModule (int analogPortNumber)
    {
        sonarAnalogInput = new AnalogInput(analogPortNumber);
    }

    public double getRawValue (){
        return sonarAnalogInput.getAverageVoltage();
    }

    public double getInchesValue(){
        double returnValue = sonarAnalogInput.getValue() * voltageScaleFactor * INCHES_CONVERSION_FACTOR;
        return  returnValue;
    }

    public double getFeetValue(){
        double returnValue = getInchesValue() / FEET_CONVERSION_FACTOR;
        return returnValue;
    }
}
