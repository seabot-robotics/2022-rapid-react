# Overview of Mechanisms 
1. Drive Train 
2. Shooter 
3. Vertical Belt 
4. Intake 
5. Climber 


## Drive Train 
Will have just the basic Drive train setup and encoders.
### Components 
- 4 CIM Motors
- 4 VEX Speed Controllers (2 lead ones) 
- 2 Encoders


## Shooter 
Will have the motor that spins the wheels for shooting and the vertical belt that takes the ball from the horizontal belt to the shooter because both the shooter motor and the vertical belt motor have to be spinning in order to shot successfully. 
### Components 
- 1 Neo Motor (Shooter Wheels)
- 1 Spark Max Speed Controller (Shooter Wheels)


## Vertical Belt 
Will have the two motors that run the horizontal belts, which run from the intake to the vertical belt although the vertical belt is not included in the horizontal belt system. 
### Components 
- 1 Neo Motors 
- 1 Spark Max Speed Controllers


## Intake 
Will include the lowering and raising of the intake device as well as the motor that spins the wheels for intake.
### Components 
- 1 Motor (Intake Wheels) 
- 1 Speed Controller 
- Pneumatics  


## Climber 
Will have the one motor that raises and lowers the hook, and the other motor that tilts the hook. 
### Components 
- 1 Neo Motor (Raises hook)
- 1 Spark Max Speed Controller 
- Pneumatics 


## I/O Map
### DriveTrain 
- Left Lead:    VictorSPX(VEX Pro)    CANID 10
- Left Follow:  VictorSPX(VEX Pro)    CANID 11
- Right Lead:   VictorSPX(VEX Pro)    CANID 13
- Right Follow: VictorSPX(VEX Pro)    CANID 14

- Right Encoder:    A, DIO2  B, DIO3
- Left Encoder:     A, DIO4, B, DIO5

- Gyro: SPI

### Ball Handling (Motor direction and speed TBD)
- Ball Intake:  Spark   PWM5
- Ball Queuer:  Spark   PWM6 
(Note to software dev: These motors are to run concurrently – Note to build: Ball Queuer motor connects to a 30 Amp branch circuit, 12 gauge wire will be used)

- Intake Raise/Lower Cylinder 1:    Solenoid 0, 1 - 0 Extend, 1 Retract
- Intake Raise/Lower Cylinder 2:    Solenoid 2, 3 - 2 Extend, 3 Retract

- Elevator/Feed:    Spark Max/Neo   CANID 20
- Shooter:          Spark Max/Neo   CANID 21

- Ball Queue Position:  Photoswitch DIO6 
Note to build: 
Assemble cable to provide power to both photoswitch modules from the PDB 5 Amp branch circuit, 16 gauge wire, red/black to 12v, white wire to DIO port, remaining wires cut off, do not trim unit cable lengths (use “burritos”).

### Climber/Hanger
- Elevator Motor:   Spark Max/Neo    CANID 22? - Need to configure SparkMax
- TiltCylinder:     Solenoid 4, 5 – 4 Extend, 5 Retract

### Miscellaneous items
- Sonar Module:         Analog 1 (Note to software dev, Electrical Team has distance test results if you need them)
- Blinkin Light String: PWM7
- Pneumatics Control Module (Note: 3 cylinders total)



